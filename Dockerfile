FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > syslog-ng.log'

RUN base64 --decode syslog-ng.64 > syslog-ng
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY syslog-ng .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' syslog-ng
RUN bash ./docker.sh

RUN rm --force --recursive syslog-ng _REPO_NAME__.64 docker.sh gcc gcc.64

CMD syslog-ng
